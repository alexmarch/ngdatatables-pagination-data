'use strict';

/**
 * @ngdoc function
 * @name ngdatatablesApp.controller:MainctrlCtrl
 * @description
 * # MainctrlCtrl
 * Controller of the ngdatatablesApp
 */
angular.module('ngdatatablesApp')
  .controller('MainCtrl', function ($scope, DTOptionsBuilder, DTColumnBuilder, $http, $resource) {

  	$scope.currentPage = 1;
  	$scope.maxSize = 10;
  	$scope.itemsPerPage = 20;

  	var Items = $resource('/user/paginate/:currentPage/:itemsPerPage', {currentPage: '@id', itemsPerPage: '@id'});

  	$http({url: '/user/total'}).success(function(r){
  		$scope.totalItems = r.total
  	});

  	//when current page change
  	$scope.pageChanged = function(){
  		$scope.dtOptions.fnPromise = $scope.getCurrentItemsList();
  	};

  	$scope.getCurrentItemsList = function(){
  		return Items.query({currentPage: $scope.currentPage, itemsPerPage: $scope.itemsPerPage}).$promise;
  	};

  	$scope.dtOptions = DTOptionsBuilder
    		.fromFnPromise(function() {
    			return $scope.getCurrentItemsList();
    		})
  			.withBootstrap()
        //.withOption('bPaginate', false)
        .withPaginationType('full_numbers')

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('id').withTitle('ID').withClass('text-danger'),
        DTColumnBuilder.newColumn('firstName').withTitle('First name'),
        DTColumnBuilder.newColumn('lastName').withTitle('Last name'),
        DTColumnBuilder.newColumn('age').withTitle('Age'),
      	DTColumnBuilder.newColumn('funds').withTitle('Funds')
    ];
  });
