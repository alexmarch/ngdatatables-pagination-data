'use strict';

/**
 * @ngdoc overview
 * @name ngdatatablesApp
 * @description
 * # ngdatatablesApp
 *
 * Main module of the application.
 */
angular
  .module('ngdatatablesApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'datatables',
    'ui.bootstrap'
  ])
  .config(function ($routeProvider) {
    // $routeProvider
    //   .when('/', {
    //     templateUrl: 'views/main.html',
    //     controller: 'MainCtrl'
    //   })
    //   .when('/about', {
    //     templateUrl: 'views/about.html',
    //     controller: 'AboutCtrl'
    //   })
    //   .otherwise({
    //     redirectTo: '/'
    //   });
  });
