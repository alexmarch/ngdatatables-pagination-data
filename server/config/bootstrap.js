/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#/documentation/reference/sails.config/sails.config.bootstrap.html
 */

module.exports.bootstrap = function(cb) {

  // It's very important to trigger this callback method when you are finished
  // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)
  User.create([{
  	firstName: 'Alex',
  	lastName: 'Marchenko',
  	age: 27,
  	funds: 34.56
  },{
  	firstName: 'Smit',
  	lastName: 'Shouh',
  	age: 30,
  	funds: 35.56,
  },{
  	firstName: 'TestFirstName3',
  	lastName: 'TestLastName3',
  	age: 32,
  	funds: 54.56,
  },{
  	firstName: 'TestFirstName4',
  	lastName: 'TestLastName4',
  	age: 28,
  	funds: 100,
  },{
  	firstName: 'TestFirstName5',
  	lastName: 'TestLastName5',
  	age: 32,
  	funds: 154.56,
  },{
  	firstName: 'Alex',
  	lastName: 'Marchenko',
  	age: 27,
  	funds: 34.56
  },{
  	firstName: 'Smit',
  	lastName: 'Shouh',
  	age: 30,
  	funds: 35.56,
  },{
  	firstName: 'TestFirstName3',
  	lastName: 'TestLastName3',
  	age: 32,
  	funds: 54.56,
  },{
  	firstName: 'TestFirstName4',
  	lastName: 'TestLastName4',
  	age: 28,
  	funds: 100,
  },{
  	firstName: 'TestFirstName5',
  	lastName: 'TestLastName5',
  	age: 32,
  	funds: 154.56,
  },{
  	firstName: 'Alex',
  	lastName: 'Marchenko',
  	age: 27,
  	funds: 34.56
  },{
  	firstName: 'Smit',
  	lastName: 'Shouh',
  	age: 30,
  	funds: 35.56,
  },{
  	firstName: 'TestFirstName3',
  	lastName: 'TestLastName3',
  	age: 32,
  	funds: 54.56,
  },{
  	firstName: 'TestFirstName4',
  	lastName: 'TestLastName4',
  	age: 28,
  	funds: 100,
  },{
  	firstName: 'TestFirstName5',
  	lastName: 'TestLastName5',
  	age: 32,
  	funds: 154.56,
  },{
  	firstName: 'Alex',
  	lastName: 'Marchenko',
  	age: 27,
  	funds: 34.56
  },{
  	firstName: 'Smit',
  	lastName: 'Shouh',
  	age: 30,
  	funds: 35.56,
  },{
  	firstName: 'TestFirstName3',
  	lastName: 'TestLastName3',
  	age: 32,
  	funds: 54.56,
  },{
  	firstName: 'TestFirstName4',
  	lastName: 'TestLastName4',
  	age: 28,
  	funds: 100,
  },{
  	firstName: 'TestFirstName5',
  	lastName: 'TestLastName5',
  	age: 32,
  	funds: 154.56,
  },{
  	firstName: 'Alex',
  	lastName: 'Marchenko',
  	age: 27,
  	funds: 34.56
  },{
  	firstName: 'Smit',
  	lastName: 'Shouh',
  	age: 30,
  	funds: 35.56,
  },{
  	firstName: 'TestFirstName3',
  	lastName: 'TestLastName3',
  	age: 32,
  	funds: 54.56,
  },{
  	firstName: 'TestFirstName4',
  	lastName: 'TestLastName4',
  	age: 28,
  	funds: 100,
  },{
  	firstName: 'TestFirstName5',
  	lastName: 'TestLastName5',
  	age: 32,
  	funds: 154.56,
  },{
  	firstName: 'Alex',
  	lastName: 'Marchenko',
  	age: 27,
  	funds: 34.56
  },{
  	firstName: 'Smit',
  	lastName: 'Shouh',
  	age: 30,
  	funds: 35.56,
  },{
  	firstName: 'TestFirstName3',
  	lastName: 'TestLastName3',
  	age: 32,
  	funds: 54.56,
  },{
  	firstName: 'TestFirstName4',
  	lastName: 'TestLastName4',
  	age: 28,
  	funds: 100,
  },{
  	firstName: 'TestFirstName5',
  	lastName: 'TestLastName5',
  	age: 32,
  	funds: 154.56,
  },{
  	firstName: 'Alex',
  	lastName: 'Marchenko',
  	age: 27,
  	funds: 34.56
  },{
  	firstName: 'Smit',
  	lastName: 'Shouh',
  	age: 30,
  	funds: 35.56,
  },{
  	firstName: 'TestFirstName3',
  	lastName: 'TestLastName3',
  	age: 32,
  	funds: 54.56,
  },{
  	firstName: 'TestFirstName4',
  	lastName: 'TestLastName4',
  	age: 28,
  	funds: 100,
  },{
  	firstName: 'TestFirstName5',
  	lastName: 'TestLastName5',
  	age: 32,
  	funds: 154.56,
  },{
  	firstName: 'Alex',
  	lastName: 'Marchenko',
  	age: 27,
  	funds: 34.56
  },{
  	firstName: 'Smit',
  	lastName: 'Shouh',
  	age: 30,
  	funds: 35.56,
  },{
  	firstName: 'TestFirstName3',
  	lastName: 'TestLastName3',
  	age: 32,
  	funds: 54.56,
  },{
  	firstName: 'TestFirstName4',
  	lastName: 'TestLastName4',
  	age: 28,
  	funds: 100,
  },{
  	firstName: 'TestFirstName5',
  	lastName: 'TestLastName5',
  	age: 32,
  	funds: 154.56,
  },{
  	firstName: 'Alex',
  	lastName: 'Marchenko',
  	age: 27,
  	funds: 34.56
  },{
  	firstName: 'Smit',
  	lastName: 'Shouh',
  	age: 30,
  	funds: 35.56,
  },{
  	firstName: 'TestFirstName3',
  	lastName: 'TestLastName3',
  	age: 32,
  	funds: 54.56,
  },{
  	firstName: 'TestFirstName4',
  	lastName: 'TestLastName4',
  	age: 28,
  	funds: 100,
  },{
  	firstName: 'TestFirstName5',
  	lastName: 'TestLastName5',
  	age: 32,
  	funds: 154.56,
  },{
  	firstName: 'Alex',
  	lastName: 'Marchenko',
  	age: 27,
  	funds: 34.56
  },{
  	firstName: 'Smit',
  	lastName: 'Shouh',
  	age: 30,
  	funds: 35.56,
  },{
  	firstName: 'TestFirstName3',
  	lastName: 'TestLastName3',
  	age: 32,
  	funds: 54.56,
  },{
  	firstName: 'TestFirstName4',
  	lastName: 'TestLastName4',
  	age: 28,
  	funds: 100,
  },{
  	firstName: 'TestFirstName5',
  	lastName: 'TestLastName5',
  	age: 32,
  	funds: 154.56,
  },{
    firstName: 'TestFirstName5',
    lastName: 'TestLastName5',
    age: 32,
    funds: 154,
  }]).exec(function(err,users){
  	if(err) sails.log.error(err);
  	cb();
  });
  //cb();
};
