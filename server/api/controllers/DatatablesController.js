/**
 * DatatablesController
 *
 * @description :: Server-side logic for managing datatables
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	get: function(req, res, next){
		User.find().exec(function(err, usersList){
			if(err) return next(err);
			return res.json(usersList);
		});
	},
	paginate: function(req, res, next){
		//@todo: Return next users by pagination list
		User
		.find()
		.paginate({page: req.param('currentPage'), limit: req.param('itemsPerPage')})
		.exec(function(err, list){
			if(err) return next(err);
			return res.json(list);
		});
	},
	total: function(req, res, next){
		User
		.count()
		.exec(function(err, total){
			if(err) return next(err);
			return res.json({total:total});
		});
	}
};

