#### Installation requirements

* Node.JS >= v0.10.22 
* npm >= 1.3.14

#### Frameworks / tools

* Sails.JS >= 0.10.4
* yo >= 1.1.2
* grunt

# Installation
First you need to install nodejs and npm
``` bash
npm install -g sails
```
``` bash
git clone git@bitbucket.org:alexmarch/ngdatatables-pagination-data.git

cd ngdatatables-pagination-data/server 

npm install

```
After installation you can run sails app http://localhost:1337
``` bash
sails lift
``` 